def calc_factorial(numero):
    factorial = 1
    for i in range(1, numero + 1):
    	factorial *=i
    
    return factorial


if __name__ == "__main__":
    for i in range(1, 11):
        print("Factorial de " + str(i) + " es " + str(calc_factorial(i)))
